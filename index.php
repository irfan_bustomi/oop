<?php
    require_once('animal.php');
    require_once('Frog.php');
    require_once('Ape.php');

    echo "Release 0 <br>";
    $sheep = new animal("shaun");
        echo "$sheep->name <br>"; // "shaun"
        echo "$sheep->legs <br>"; // 2
        echo "$sheep->cold_blooded <br><br>";// false

    echo "Release 1 <br>";
    $sungokong = new Ape("kera sakti");
        echo $sungokong->yell(); 
        echo "<br>";
    
    $kodok = new Frog("buduk");
        echo $kodok->jump();
        
?>